
var webpack = require('webpack');
var path = require('path');

const HtmlWebPackPlugin = require("html-webpack-plugin");

module.exports = {
	entry: [
		path.join(__dirname, '/src/index.js')
	],
	mode: "development",
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: 'babel-loader'
			},
			{
				test: /\.(less|css)$/,
				use: ['style-loader', 'css-loader', 'less-loader']
			}
		]
	},
	resolve: { extensions: ["*", ".js", ".jsx"] },
	output: {
		path: path.resolve(__dirname, "public/"),
		filename: "bundle.js"
	},
	plugins: [
		new HtmlWebPackPlugin({
			template: "./public/index.html",
			filename: "./public/index.html"
		}),
		new webpack.HotModuleReplacementPlugin()
	],
	devServer: {
		contentBase: path.join(__dirname, "public/"),
		port: 5001,
		historyApiFallback: true
	}
}
	