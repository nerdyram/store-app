import React, { Component } from 'react';

import { MuiThemeProvider } from "@material-ui/core/styles";
import Alert from '@material-ui/lab/Alert';
import Snackbar from '@material-ui/core/Snackbar';

import { GlobalContext } from './context';
import AdminContainer from './admin/AdminContainer';
import { theme } from './Theme';
import HomeContainer from './home/HomeContainer';
import LoginContainer from './login/LoginContainer';
import * as accountServiceConstants from '../constants/accountService';
import * as serviceInfo from '../constants/serviceInfo';
import * as messageTypes from '../constants/messageTypes';

class App extends Component {
     constructor(props) {
          super(props);
          let account = {};
          let adminOpen = false;
          let homeOpen = false;
          if (localStorage["account"]) {
               account = JSON.parse(localStorage["account"]);
               if (account.role === "ADMIN") {
                    adminOpen = true;
               } else {
                    homeOpen = true;
               }
          }
          this.state = {
               open: false,
               message: "",
               vertical: 'top',
               horizontal: 'center',
               messageType: messageTypes.INFO,
               page: {
                    homeOpen: homeOpen,
                    adminOpen: adminOpen
               },
               loginAccount: account
          }
     }
     handleOpenSnackBarMessage = (barMessage, verticalPos, horizontalPos, messageType) => {
          this.setState({
               ...this.state,
               open: true,
               message: barMessage,
               vertical: verticalPos,
               horizontal: horizontalPos,
               messageType: messageType
          });
     }

     handleClose = () => {
          this.setState({
               ...this.state,
               message: "",
               open: false,
               messageType: messageTypes.INFO
          });
     };

     handleChangePage = (homeIsOpen, adminIsOpen) => {
          this.setState({
               ...this.state,
               page: {
                    homeOpen: homeIsOpen,
                    adminOpen: adminIsOpen
               }
          });
     };

     getSnackBarMessage = (message, messageType) => {
          return (
               <Alert elevation={6} variant="filled" severity={messageType} onClose={this.handleClose}>{message}</Alert>
          )
     }

     handleUpdateLoginAccountInfo = (account) => {
          localStorage["account"] = JSON.stringify(account);
          let adminOpen = false;
          let homeOpen = false;
          if (account.role === "ADMIN") {
               adminOpen = true;
          } else {
               homeOpen = true;
          }
          this.setState({
               ...this.state,
               loginAccount: account,
               page: {
                    adminOpen : adminOpen,
                    homeOpen: homeOpen
               }
          })
     }

     handleLogout = () => {
          let url = serviceInfo.HOST + accountServiceConstants.LOGOUT_ACCOUNT_URI_PATH;
          // Request options
          let options = {
               method: 'POST',
               body: JSON.stringify({
                    username: JSON.parse(localStorage["account"]).username
               })
          };
          let self = this;
          fetch(url, options)
               .then(function (response) {
                    if (response.status == 200) {
                         self.handleOpenSnackBarMessage("Çıkış gerçekleştirildi!", "top", "center", messageTypes.SUCCESS);
                         return response.json();
                    } else {

                    }
               }).catch((error => {
                    console.error(error);
               }))
          delete localStorage["account"];
          this.setState({
               ...this.state,
               loginAccount: undefined
          })
     }

     render() {
          const {
               vertical,
               horizontal,
               open,
               message,
               messageType,
               page,
               loginAccount
          } = this.state;
          const alertMessage = this.getSnackBarMessage(message, messageType);
          return (
               <div>
                    <GlobalContext.Provider value={{
                         handleOpenSnackBarMessage: this.handleOpenSnackBarMessage,
                         handleChangePage: this.handleChangePage,
                         handleLogout: this.handleLogout
                    }}>
                         <MuiThemeProvider theme={theme}>
                              {
                                   !localStorage["account"] &&
                                   <LoginContainer
                                        handleUpdateLoginAccountInfo={this.handleUpdateLoginAccountInfo}
                                        handleOpenSnackBarMessage={this.handleOpenSnackBarMessage} />
                              }
                              {
                                   page.homeOpen && localStorage["account"] &&
                                   <HomeContainer handleOpenSnackBarMessage={this.handleOpenSnackBarMessage} />
                              }
                              {
                                   page.adminOpen && localStorage["account"] && JSON.parse(localStorage["account"]).role === 'ADMIN' &&
                                   <AdminContainer />
                              }
                         </MuiThemeProvider>
                    </GlobalContext.Provider>
                    <Snackbar
                         autoHideDuration={5000}
                         onClose={() => this.handleClose()}
                         anchorOrigin={{ vertical, horizontal }}
                         open={open}
                         key={vertical + horizontal}>
                         {alertMessage}
                    </Snackbar>
               </div>
          )
     }
}

export default App;