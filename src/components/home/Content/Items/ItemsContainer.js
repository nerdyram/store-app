import React, { Component } from 'react';

import { withStyles } from '@material-ui/core';
import { Grid } from '@material-ui/core';

import { ItemCard } from './ItemCard';
import { styles } from './Style';

class ItemsContainer extends Component {

     constructor(props) {
          super(props);
          this.state = {
               item: {},
               quickViewOpen: false
          }
     }

     createItemsGrid = (items) => {
          let itemsGrid = []
          let grid = []
          for (let itemIndex = 0; itemIndex < items.length; itemIndex++) {
               if (grid.length % 4 === 0 && grid.length !== 0) {
                    itemsGrid.push(grid);
                    grid = []
               }
               if(items[itemIndex].units.length > 0) {
                    grid.push(items[itemIndex]);
               }               
          }
          if (grid.length > 0) {
               itemsGrid.push(grid);
               grid = []
          }
          return itemsGrid;
     }

     emptyItemsGrid = () => {
          const {
               classes
          } = this.props;
          return (
               <Grid container></Grid>
          )
     }

     render() {
          const {
               items,
               handleOpenCart,
               addItemToCart
          } = this.props;
          let itemsGrid = this.createItemsGrid(items);
          return (
               <div>
                    <Grid container>
                         {
                              itemsGrid.map((itemGrid, index) => (
                                   <Grid key={index} container>
                                        {
                                             itemGrid.map((item, index) => (
                                                  <Grid key={index} item md={3}>
                                                       <ItemCard
                                                            handleOpenCart={handleOpenCart}
                                                            addItemToCart={addItemToCart}
                                                            item={item} 
                                                            />
                                                  </Grid>
                                             ))
                                        }
                                   </Grid>
                              ))
                         }
                    </Grid>
               </div>
          )
     }
}

export default withStyles(styles, { withTheme: true })
     (
          ItemsContainer
     );