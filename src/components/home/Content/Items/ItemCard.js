import React from 'react';
import { 
     Card, 
     makeStyles, 
     CardActionArea, 
     CardContent, 
     CardActions, 
     CardMedia, 
     IconButton 
} from '@material-ui/core';
import AddToCartIcon from '@material-ui/icons/AddShoppingCart';

const useStyles = makeStyles({
     root: {
          flexGrow: 1,
          margin: "8px",
          backgroundColor: "#f0f3bd"
     },
     media: {
          height: 140
     },
});

export const ItemCard = (props) => {
     const classes = useStyles();
     const {
          item,
          addItemToCart
     } = props;
     return (
          <Card className={classes.root}>
               <CardActionArea>
                    <CardMedia
                         className={classes.media}
                         image="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                         title={item.name}
                    />
                    <CardContent>
                         {item.name}
                    </CardContent>
               </CardActionArea>
               <CardActions>
                    <IconButton onClick={() => addItemToCart(item, item.units[0])}>
                         <AddToCartIcon/>
                    </IconButton>
               </CardActions>
          </Card>
     )
}