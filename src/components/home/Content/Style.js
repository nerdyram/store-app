export const styles = theme => ({
     root: {
          margin: "0 auto",
          fontSize: "18px",
          padding: "5x",
          maxWidth: "1200px",
          backgroundColor: "#02c39a"
     },
     grow: {
          flexGrow: 1
     }
});