import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

import ItemsContainer from './Items/ItemsContainer';
import { styles } from './Style';

export const Content = (props) => {
     const classes = makeStyles(styles)();
     const {
          items,
          addItemToCart
     } = props;
     return(
          <div className={classes.root}>
               <ItemsContainer addItemToCart={addItemToCart} items={items}/>
          </div>
     )
}