import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import { Toolbar, Typography, IconButton, Badge } from '@material-ui/core';
import CartIcon from '@material-ui/icons/ShoppingCart';

import { GlobalContext } from '../../context';
import SearchContainer from './Search/SearchContainer';
import CartContainer from './Cart/CartContainer';
import { styles } from './Style';

export const Banner = (props) => {
     const classes = makeStyles(styles)();
     const {
          order,
          handleSearchChange,
          handleOpenCart,
          handleCloseCart,
          cartOpen
     } = props;
     return (
          <GlobalContext.Consumer>
               {
                    globalContext => (
                         <div className={classes.root}>
                              <Toolbar variant="regular" className={classes.toolBar}>
                                   <Typography className={classes.siteName} variant="h4">
                                        Malzemeler
                                   </Typography>
                                   <Typography className={classes.grow} variant="caption">
                                        <SearchContainer handleSearchChange={handleSearchChange}/>
                                   </Typography>
                                   <Badge 
                                        className={classes.cartButton} 
                                        badgeContent={order.orderLines.length}
                                        onClick={handleOpenCart}
                                        >
                                        <IconButton>
                                             <CartIcon/>
                                        </IconButton>
                                   </Badge>
                              </Toolbar>
                              <CartContainer
                                   order={order}
                                   cartOpen={cartOpen}
                                   handleCloseCart={handleCloseCart}
                                   />
                         </div>
                    )
               }
          </GlobalContext.Consumer>
     )
}