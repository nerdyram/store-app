import React, { Component } from 'react';


import { styles } from './Style';
import { Dialog, DialogTitle, Typography, DialogContent, DialogActions, Button } from '@material-ui/core';

import { ItemsTable } from './ItemsTable';

class CartContainer extends Component {

     constructor(props) {
          super(props);
          this.state = {
               selectedOrderLine: {
                    id: 0,
                    item: {},
                    unit: {},
                    status: "Aktif",
                    quantity: 1.00 
               }
          }
     }

     render() {
          const {
               handleCreateOrder,
               handleUpdateOrderLine,
               order,
               cartOpen,
               handleCloseCart
          } = this.props;
          return (
               <Dialog open={cartOpen} maxWidth="md" onClose={handleCloseCart}>
                    <DialogTitle disableTypography={true}>
                         <Typography variant="h3">
                              Sepet
                         </Typography>
                    </DialogTitle>
                    <DialogContent>
                         <Typography variant="h4">
                              Malzemeler
                         </Typography>
                         <ItemsTable orderLines={order.orderLines}/>
                    </DialogContent>
                    <DialogActions>
                         <Button onClick={() => handleCreateOrder()} variant="contained" color="primary">
                              Sipariş Oluştur
                         </Button>
                         <Button onClick={() => handleCloseCart()} variant="contained" color="secondary">
                              Kapat
                         </Button>
                    </DialogActions>
               </Dialog>
          )
     }
}

export default (CartContainer);