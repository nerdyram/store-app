import React from 'react';
import { Table, TableHead, TableRow, TableBody, TableCell } from '@material-ui/core';

export const ItemsTable = (props) => {
     const {
          orderLines
     } = props;
     return (
          <Table>
               <TableHead>
                    <TableRow>
                         <TableCell component="th">
                              Adı
                         </TableCell>
                         <TableCell component="th">
                              Birim
                         </TableCell>
                         <TableCell component="th">
                              Miktar
                         </TableCell>
                    </TableRow>
               </TableHead>
               <TableBody>
                    {
                         orderLines.map((orderLine, index) => {
                              return (
                                   <TableRow key={index}>
                                        <TableCell component="td">
                                             {
                                                  orderLine.item.name
                                             }
                                        </TableCell>
                                        <TableCell component="td">
                                             {
                                                  orderLine.unit.name
                                             }
                                        </TableCell>
                                        <TableCell component="td">
                                             {
                                                  orderLine.quantity
                                             }
                                        </TableCell>
                                   </TableRow>
                              )
                         })
                    }
               </TableBody>
          </Table>
     )
}