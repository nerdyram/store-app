export const styles = theme => ({
     root: {
          margin: "0 auto",
          fontSize: "18px",
          padding: "5x",
          maxWidth: "1200px"
     },
     toolBar: {
          backgroundColor: "#028090",
          height: "20px"
     },
     grow: {
          flexGrow: 1
     },
     siteName: {
          cursor: "pointer",
          marginLeft: "20px",
          marginRight: "20px",
          color: "#FFFFFF"
     },
     cartButton: {
          margin: "20px",
     }
});