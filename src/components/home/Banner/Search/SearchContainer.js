import React, { Component } from 'react';

import { withStyles } from '@material-ui/core/styles';
import { FormControl, Input } from '@material-ui/core';

import { styles } from './Style';

class SearchContainer extends Component {

     constructor(props) {
          super(props);
     }

     render() {
          const {
               classes,
               handleSearchChange
          } = this.props;
          return (
               <FormControl fullWidth variant="outlined" className={classes.root}>
                    <Input className={classes.searctInput} id="searchInput" placeholder={"Bandaj"} onChange={(e) => handleSearchChange(e) }/>
               </FormControl>
          )
     }
}

export default withStyles(styles, { withTheme: true })(
     SearchContainer
)