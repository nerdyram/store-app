import React, { Component } from 'react';

import { Header } from './Header/';
import { Banner } from './Banner/';
import { Content } from './Content/';

import * as serviceInfo from '../../constants/serviceInfo';
import * as itemServiceConstants from '../../constants/itemService';
import * as orderServiceConstants from '../../constants/orderService';
import * as messageTypes from '../../constants/messageTypes';

class HomeContainer extends Component {

     constructor(props) {
          super(props)
          this.state = {
               order: {
                    id: 0,
                    orderLines: [],
                    status: "AT_CART",
                    customer: JSON.parse(localStorage["account"])
               },
               items: [],
               cartOpen: false
          }
     }
     componentDidMount = () => {
          this.applySearch("");
          this.getLastOrderAtCart();
     }

     applySearch = (search) => {
          const self = this;
          let url = serviceInfo.HOST + itemServiceConstants.SEARCH_ITEM_URI_PATH;
          // Request options
          let options = {
               method: 'POST',
               body: JSON.stringify({ search: search })
          };

          fetch(url, options)
               .then(function (response) {
                    return response.json();
               }).then((json) => {
                    self.setState({
                         ...self.state,
                         items: json.data
                    })
               }).catch((error => {
                    console.info(error)
                    self.setState({
                         ...self.state,
                         items: []
                    })
               }))
     }

     getLastOrderAtCart = () => {
          const self = this;
          let url = serviceInfo.HOST + orderServiceConstants.GET_LAST_ORDER_AT_CART_URI_PATH;
          let options = {
               method: 'GET'
          };
          fetch(url, options)
               .then(function (response) {
                    return response.json();
               }).then((json) => {
                    let order = json.data;
                    console.info(order);
                    if (order.id !== 0) {
                         self.setState({
                              ...self.state,
                              order: json.data
                         })
                    } else {
                         self.setState({
                              ...self.state,
                              order: {
                                   id: 0,
                                   orderLines: [],
                                   status: "AT_CART",
                                   customer: JSON.parse(localStorage["account"])
                              }
                         })
                    }
               }).catch((error => {
                    self.setState({
                         ...self.state
                    })
               }))
     }

     handleSearchChange = (e) => {
          this.applySearch(e.target.value);
     }

     addItemToCart = (item, unit) => {
          const {
               handleOpenSnackBarMessage
          } = this.props;
          let orderLine = {
               id: 0,
               item: item,
               unit: unit,
               status: "Aktif",
               quantity: 1.00 
          }
          let order = this.state.order;
          const self = this;
          let url = serviceInfo.HOST + orderServiceConstants.CREATE_ORDER_URI_PATH;
          if (order.id !== 0) {
               url = serviceInfo.HOST + orderServiceConstants.UPDATE_ORDER_URI_PATH;
          }
          // check if item already exists in orderLines
          let orderLines = order.orderLines;
          let itemFound = false;
          for (let orderLineIndex = 0; orderLineIndex < orderLines.length; orderLineIndex++) {
               if(item.id === orderLines[orderLineIndex].item.id) {
                    orderLines[orderLineIndex].quantity += 1;
                    itemFound = true;
                    break;
               }
          }
          if (!itemFound) {
               orderLines.push(orderLine);
          }
          order.orderLines = orderLines;
          
          // Request options
          let options = {
               method: 'POST',
               body: JSON.stringify(order)
          };
          fetch(url, options)
               .then(function (response) {
                    return response.json();
               }).then((json) => {
                    handleOpenSnackBarMessage("Ürün sepete eklendi!", "top", "center", messageTypes.SUCCESS)
                    self.setState({
                         ...self.state,
                         order: json.data
                    })
               }).catch((error => {
                    handleOpenSnackBarMessage("Ürün sepet eklenemedi!", "top", "center", messageTypes.ERROR)
                    self.setState({
                         ...self.state
                    })
               }))
     }

     createOrder = () => {

     }

     handleOpenCart = () => {
          this.setState({
               ...this.state,
               cartOpen: true
          })
     }

     handleCloseCart = () => {
          this.setState({
               ...this.state,
               cartOpen: false
          })
     }

     render() {
          return (
               <div>
                    <Header/>
                    <Banner 
                         handleSearchChange={this.handleSearchChange}
                         handleOpenCart={this.handleOpenCart}
                         handleCloseCart={this.handleCloseCart}
                         order={this.state.order}
                         cartOpen={this.state.cartOpen}
                         />
                    <Content 
                         items={this.state.items}
                         addItemToCart={this.addItemToCart}
                         />
                    { /* Banner { Logo/Name, Search, Cart } */ }
                    { /* Content{ Items } */ }
                    { /* Footer { Contact Information, Social Media }*/ }
               </div>
          )
     }
}

export default HomeContainer;