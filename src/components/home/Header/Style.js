export const styles = theme => ({
     root: {
          margin: "0 auto",
          fontSize: "18px",
          padding: "5x",
          maxWidth: "1200px"
     },
     toolBar: {
          backgroundColor: "#05668D",
          height: "20px"
     },
     grow: {
          flexGrow: 1
     },
     link: {
          cursor: "pointer",
          marginLeft: "20px",
          marginRight: "20px",
          color: "#FFFFFF"
     }
});