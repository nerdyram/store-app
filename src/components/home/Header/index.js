import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import { Toolbar, Typography, Link, Button } from '@material-ui/core';
import LogoutIcon from '@material-ui/icons/ExitToApp';

import { GlobalContext } from '../../context';
import { styles } from './Style';

export const Header = (props) => {
     const classes = makeStyles(styles)();
     return (
          <GlobalContext.Consumer>
               {
                    globalContext => (
                         <div className={classes.root}>
                              <Toolbar variant="regular" className={classes.toolBar}>
                                   <Link className={classes.link} onClick={() => globalContext.handleChangePage(false, true)}>
                                        <Typography>
                                             Yönetim
                                        </Typography>
                                   </Link>
                                   <Typography className={classes.grow}>

                                   </Typography>
                                   <Link className={classes.link}>
                                        <Typography>
                                             Hesap
                                        </Typography>
                                   </Link>
                                   <Button className={classes.link} color="secondary" onClick={() => globalContext.handleLogout()}>
                                        Çıkış Yap
                                        <LogoutIcon/>
                                   </Button>
                              </Toolbar>
                         </div>
                    )
               }
          </GlobalContext.Consumer>
     )
}