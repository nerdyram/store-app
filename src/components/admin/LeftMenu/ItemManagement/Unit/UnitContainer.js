import React, { Component } from 'react';
import { cloneDeep } from 'lodash';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import { withStyles } from '@material-ui/core/styles';

import { styles } from './Style';
import { UnitTable } from './UnitTable';
import * as serviceInfo from '../../../../../constants/serviceInfo';
import * as itemServiceConstants from '../../../../../constants/itemService';
import * as messageTypes from '../../../../../constants/messageTypes';
import { NewUnitDialog } from './dialogs/NewUnitDialog';
import { EditUnitDialog } from './dialogs/EditUnitDialog';

class UnitContainer extends Component {

     constructor(props) {
          super(props);
          this.state = {
               units: [],
               unit: {
                    id: 0,
                    name: "",
                    status: "Aktif"
               },
               selectedUnit: {},
               rowsPerPage: 5,
               page: 0,
               dialog: {
                    editOpen: false,
                    createOpen: false
               }
          }
     }

     componentDidMount = () => {
          this.listUnits();
     }

     listUnits = () => {
          const self = this;
          const {
               handleOpenSnackBarMessage
          } = self.props;
          let url = serviceInfo.HOST + itemServiceConstants.LIST_UNIT_URI_PATH;
          // Request options
          let options = {
               method: 'GET'
          };

          fetch(url, options)
               .then(function (response) {
                    return response.json();
               }).then((json) => {
                    self.setState({
                         ...self.state,
                         units: json.data,
                         unit: {
                              id: 0,
                              name: "",
                              status: "Aktif"
                         },
                         selectedUnit: {
                              id: 0,
                              name: "",
                              status: "Aktif"
                         },
                         dialog: {
                              createOpen: false,
                              editOpen: false
                         }
                    })
               }).catch((error => {
                    handleOpenSnackBarMessage("Birimleri sorgularken hata oluştu!", "top", "center", messageTypes.ERROR);
                    self.setState({
                         ...self.state,
                         units: []
                    })
               }))
     }

     handleChangePage = (event, page) => {
          this.setState({
               ...this.state,
               page: page
          });
     }

     handleChangeRowsPerPage = (event, page) => {
          setRowsPerPage(parseInt(event.target.value, 10));
          this.setState({
               ...this.state,
               rowsPerPage: parseInt(event.target.value, 10),
               page: page
          });
     }

     handleOpenNewUnitDialog = () => {
          this.setState({
               ...this.state,
               unit: {
                    id: 0,
                    name: "",
                    status: "Aktif"
               },
               dialog: {
                    ...this.state.dialog,
                    createOpen: true
               }
          })
     }

     handleInfoChange = (e, field, isCheckBox) => {
          let changed = this.state.unit;
          if (!isCheckBox) {
               changed[field] = e.target.value;
          } else {
               changed[field] = e.target.checked;
          }

          this.setState({
               ...this.state,
               unit: changed
          })
     }

     handleCreate = () => {
          const self = this;
          const {
               handleOpenSnackBarMessage
          } = this.props;
          let url = serviceInfo.HOST + itemServiceConstants.CREATE_UNIT_URI_PATH;
          // Request options
          let options = {
               method: 'POST',
               body: JSON.stringify(self.state.unit)
          };

          fetch(url, options)
               .then(function (response) {
                    if (response.status === 200) {
                         handleOpenSnackBarMessage("Birim kayıt edildi!", "top", "center", messageTypes.SUCCESS);
                         self.listUnits();
                    }
               })
               .catch((error => {
                    handleOpenSnackBarMessage(error, "top", "center", messageTypes.ERROR);
                    self.setState({
                         ...self.state,
                         unit: {
                              id: 0,
                              name: "",
                              status: "Aktif"
                         },
                         dialog: {
                              ...self.state.dialog,
                              createOpen: false
                         }
                    })
               }))
     }

     handleCancelNewUnitDialog = () => {
          this.setState({
               ...this.state,
               unit: {
                    id: 0,
                    name: "",
                    status: "Aktif"
               },
               dialog: {
                    ...this.state.dialog,
                    createOpen: false
               }
          })
     }

     handleEditUnitDialogOpen = (unit) => {
          let unitToUpdate = cloneDeep(unit);
          this.setState({
               ...this.state,
               unit: unitToUpdate,
               dialog: {
                    ...this.state.dialog,
                    editOpen: true
               }
          })
     }

     handleCancelEditUnitDialog = () => {
          this.setState({
               ...this.state,
               unit: {
                    id: 0,
                    name: "",
                    status: "Aktif"
               },
               dialog: {
                    ...this.state.dialog,
                    editOpen: false
               }
          })
     }

     handleUpdate = () => {
          const self = this;
          const {
               handleOpenSnackBarMessage
          } = self.props;
          let url = serviceInfo.HOST + itemServiceConstants.UPDATE_UNIT_URI_PATH;
          // Request options
          let options = {
               method: 'POST',
               body: JSON.stringify(this.state.unit)
          };
          fetch(url, options)
               .then(function (response) {
                    if (response.status === 200) {
                         handleOpenSnackBarMessage("Birim güncellendi!", "top", "center", messageTypes.SUCCESS);
                         self.listUnits();
                    }
               })
               .catch((error => {
                    handleOpenSnackBarMessage(error, "top", "center", messageTypes.ERROR);
                    self.setState({
                         ...self.state,
                         unit: {
                              id: 0,
                              name: "",
                              status: "Aktif"
                         },
                         dialog: {
                              ...self.state.dialog,
                              editOpen: false
                         }
                    })
               }))
          
     }

     handleSelectedUnit = (unitId) => {
          const {
               units
          } = this.state;
          if (units.length > 0) {
               let unitsLength = units.length;
               for(let unitIndex = 0; unitIndex < unitsLength; unitIndex++) {
                    if (unitId === units[unitIndex].id) {
                         this.setState({
                              ...this.state,
                              selectedUnit: units[unitIndex]
                         })
                         break;
                    }
               }
          }
     }

     render() {
          const {
               units,
               rowsPerPage,
               page,
               dialog,
               selectedUnit
          } = this.state;
          const {
               classes
          } = this.props;
          return (
               <div>
                    <Grid container>
                         <Grid item md={12}>
                              <Button onClick={this.handleOpenNewUnitDialog} variant="contained" color="primary">
                                   Yeni Birim
                              </Button>
                         </Grid>
                         <Grid item md={12}>
                              <UnitTable 
                                   classes={classes}
                                   units={units}
                                   rowsPerPage={rowsPerPage}
                                   page={page}
                                   selectedUnit={selectedUnit}
                                   handleEditUnitDialogOpen={this.handleEditUnitDialogOpen}
                                   handleChangePage={this.handleChangePage}
                                   handleChangeRowsPerPage={this.handleChangeRowsPerPage}
                                   handleSelectedUnit={this.handleSelectedUnit}
                                   />
                         </Grid>
                    </Grid>
                    
                    <NewUnitDialog
                         open={dialog.createOpen}
                         unit={this.state.unit}
                         handleInfoChange={this.handleInfoChange}
                         handleCreate={this.handleCreate}
                         handleCancel={this.handleCancelNewUnitDialog}
                    />
                    <EditUnitDialog
                         open={dialog.editOpen}
                         unit={this.state.selectedUnit}
                         handleInfoChange={this.handleInfoChange}
                         handleUpdate={() => this.handleUpdate()}
                         handleCancel={this.handleCancelEditUnitDialog}
                    />
               </div>
          )
     }
}

export default withStyles(styles, { withTheme: true })(
     UnitContainer
);