import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import {
     Dialog,
     DialogContent, 
     DialogActions, 
     Button,
     FormControl, 
     InputLabel, 
     Select, 
     OutlinedInput
} from '@material-ui/core';

import { styles } from './Style';

export const NewUnitDialog = (props) => {
     const classes = makeStyles(styles)();
     const {
          open,
          unit,
          handleInfoChange,
          handleCreate,
          handleCancel
     } = props;
     console.info(unit);
     return (
          <Dialog
               open={open}
               fullWidth={true}
               maxWidth="sm"
               >
               <DialogContent>
                    <form className={classes.root}>
                         <FormControl margin={"normal"} fullWidth>
                              <InputLabel htmlFor="itemName">Adı</InputLabel>
                              <OutlinedInput
                                   id="itemName"
                                   value={unit.name}
                                   onChange={(e) => handleInfoChange(e, "name", false)}
                                   />  
                         </FormControl>
                         <FormControl margin={"normal"} fullWidth>
                              <InputLabel htmlFor="statusSelect">Durum</InputLabel>
                              <Select
                                   variant={"outlined"}
                                   native
                                   value={unit.status}
                                   onChange={(e) => handleInfoChange(e, "status", false)}
                                   inputProps={{
                                   name: 'status',
                                   id: 'statusSelect',
                                   }}
                              >
                                   <option value={"Aktif"}>Aktif</option>
                                   <option value={"Pasif"}>Pasif</option>
                              </Select>
                         </FormControl>                   
                    </form>
               </DialogContent>
               <DialogActions>
                    <Button onClick={handleCreate} variant="contained" color="primary">
                         Kaydet
                    </Button>
                    <Button onClick={handleCancel} variant="contained" color="secondary">
                         İptal
                    </Button>
               </DialogActions>
          </Dialog>
     )
}