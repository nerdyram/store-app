import React from 'react';

// Table Components
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import { TablePaginationActions } from '../../../common/TablePaginationActions';

export const ItemTable = (props) => {
     const {
          classes,
          items,
          rowsPerPage,
          page,
          selectedItem,
          handleEditItemDialogOpen,
          handleChangePage,
          handleChangeRowsPerPage,
          handleSelectedItem
     } = props;
     return (
          <TableContainer component={Paper}>
               <Table className={classes.table} aria-label="items table">
                    <TableHead >
                         <TableRow>
                              <TableCell align="right">Adı</TableCell>
                              <TableCell align="right">Reçete Gerekiyor?</TableCell>
                              <TableCell align="right">Durum</TableCell>
                         </TableRow>
                    </TableHead>
                    <TableBody>
                         {
                              (rowsPerPage > 0 ? items.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage) : items)
                                   .map((item) => (
                                        <TableRow
                                             role="checkbox"
                                             style={{height: 33}} 
                                             key={item.id} 
                                             onDoubleClick={() => handleEditItemDialogOpen(item)}
                                             onClick={(e) => handleSelectedItem(e, item.id)}
                                             selected={selectedItem.id === item.id}
                                             >
                                             <TableCell align="right" component="th" scope="row">
                                                  {item.name}
                                             </TableCell>
                                             <TableCell align="right">
                                                  {item.requiresRecipe ? "Evet" : "Hayır"}
                                             </TableCell>
                                             <TableCell align="right">
                                                  {item.status}
                                             </TableCell>
                                        </TableRow>
                                   ))}
                    </TableBody>
                    <TableFooter>
                         <TableRow>
                              <TablePagination
                                   rowsPerPageOptions={[5, 10, 25, { label: 'Hepsi', value: -1 }]}
                                   colSpan={3}
                                   count={items.length}
                                   rowsPerPage={rowsPerPage}
                                   page={page}
                                   SelectProps={{
                                        inputProps: { 'aria-label': 'rows per page' },
                                        native: true,
                                   }}
                                   onChangePage={handleChangePage}
                                   onChangeRowsPerPage={handleChangeRowsPerPage}
                                   ActionsComponent={TablePaginationActions}
                              />
                         </TableRow>
                    </TableFooter>
               </Table>
          </TableContainer>
     )
}