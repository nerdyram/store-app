import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import {
     Dialog,
     DialogContent, 
     DialogActions, 
     Button,
     Checkbox,
     FormControlLabel, 
     FormControl, 
     InputLabel, 
     Select, 
     OutlinedInput
} from '@material-ui/core';

import { styles } from './Style';

export const NewItemDialog = (props) => {
     const classes = makeStyles(styles)();
     const {
          open,
          item,
          handleItemInfoChange,
          handleCreateItem,
          handleCancel
     } = props;

     return (
          <Dialog
               open={open}
               fullWidth={true}
               maxWidth="sm"
               >
               <DialogContent>
                    <form className={classes.root}>
                         <FormControl margin={"normal"} fullWidth>
                              <InputLabel htmlFor="itemName">Adı</InputLabel>
                              <OutlinedInput
                                   id="itemName"
                                   value={item.name}
                                   onChange={(e) => handleItemInfoChange(e, "name", false)}
                                   />  
                         </FormControl>
                         <FormControl margin={"normal"} fullWidth>
                              <InputLabel htmlFor="statusSelect">Durum</InputLabel>
                              <Select
                                   variant={"outlined"}
                                   native
                                   value={item.status}
                                   onChange={(e) => handleItemInfoChange(e, "status", false)}
                                   inputProps={{
                                   name: 'status',
                                   id: 'statusSelect',
                                   }}
                              >
                                   <option value={"Aktif"}>Aktif</option>
                                   <option value={"Pasif"}>Pasif</option>
                              </Select>
                         </FormControl>   
                         <FormControl margin={"normal"} fullWidth>
                              <FormControlLabel id="itemRequiresRecipe"
                                   control={
                                        <Checkbox
                                             checked={item.requiresRecipe}
                                             onClick={(e) => handleItemInfoChange(e, "requiresRecipe", true)}
                                             color="primary"
                                             />
                                   }
                                   label="Reçete Gerekiyor"
                                   />
                         </FormControl>                
                    </form>
               </DialogContent>
               <DialogActions>
                    <Button onClick={handleCreateItem} variant="contained" color="primary">
                         Kaydet
                    </Button>
                    <Button onClick={handleCancel} variant="contained" color="secondary">
                         İptal
                    </Button>
               </DialogActions>
          </Dialog>
     )
}