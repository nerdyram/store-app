import React, { Component } from 'react';
import { cloneDeep } from 'lodash';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import { withStyles } from '@material-ui/core/styles';

import { ItemTable } from './ItemTable';
import * as serviceInfo from '../../../../../constants/serviceInfo';
import * as itemServiceConstants from '../../../../../constants/itemService';
import * as messageTypes from '../../../../../constants/messageTypes';
import { styles } from './Style';
import { NewItemDialog } from './dialogs/NewItemDialog';
import { EditItemDialog } from './dialogs/EditItemDialog';
import { ItemUnitAssociation } from './ItemUnitAssociation';
import { Divider } from '@material-ui/core';

class ItemContainer extends Component {

     constructor(props) {
          super(props);
          this.state = {
               items: [],
               item: {
                    id: 0,
                    name: "",
                    requiresRecipe: false,
                    status: "Aktif",
                    units: []
               },
               selectedItem: {},
               itemsNeedToBeFetched: false,
               rowsPerPage: 5,
               page: 0,
               dialog: {
                    editOpen: false,
                    createOpen: false
               },
               units: []
          }
     }

     componentDidMount = () => {
          this.listItems();
          this.getUnits();
     }

     listItems = () => {
          const {
               handleOpenSnackBarMessage
          } = this.props;
          let url = serviceInfo.HOST + itemServiceConstants.LIST_ITEM_URI_PATH;
          // Request options
          let options = {
               method: 'GET'
          };

          fetch(url, options)
               .then(function (response) {
                    return response.json();
               }).then((json) => {
                    this.setState({
                         ...this.state,
                         items: json.data,
                         item: {
                              id: 0,
                              name: "",
                              requiresRecipe: false,
                              status: "Aktif",
                              units: []
                         },
                         dialog: {
                              createOpen: false,
                              editOpen: false
                         }
                    })
               }).catch((error => {
                    handleOpenSnackBarMessage(error, "top", "center", messageTypes.ERROR)
                    this.setState({
                         ...this.state,
                         items: []
                    })
               }))
     }

     getUnits = () => {
          const {
               handleOpenSnackBarMessage
          } = this.props;
          let url = serviceInfo.HOST + itemServiceConstants.LIST_UNIT_URI_PATH;
          // Request options
          let options = {
               method: 'GET'
          };

          fetch(url, options)
               .then(function (response) {
                    return response.json();
               }).then((json) => {
                    this.setState({
                         ...this.state,
                         units: json.data,
                         dialog: {
                              createOpen: false,
                              editOpen: false
                         }
                    })
               }).catch((error => {
                    handleOpenSnackBarMessage(error, "top", "center", messageTypes.ERROR)
                    this.setState({
                         ...this.state,
                         items: []
                    })
               }))
     }

     handleChangePage = (event, page) => {
          this.setState({
               ...this.state,
               page: page
          });
     }

     handleChangeRowsPerPage = (event, page) => {
          setRowsPerPage(parseInt(event.target.value, 10));
          this.setState({
               ...this.state,
               rowsPerPage: parseInt(event.target.value, 10),
               page: page
          });
     }

     handleOpenNewItemDialog = () => {
          this.setState({
               ...this.state,
               item: {
                    id: 0,
                    name: "",
                    requiresRecipe: false,
                    status: "Aktif",
                    units: []
               },
               dialog: {
                    ...this.state.dialog,
                    createOpen: true
               }
          })
     }

     handleItemInfoChange = (e, field, isCheckBox) => {
          let changedItem = this.state.item;
          if (!isCheckBox) {
               changedItem[field] = e.target.value;
          } else {
               changedItem[field] = e.target.checked;
          }

          this.setState({
               ...this.state,
               item: changedItem
          })
     }

     handleCreateItem = () => {
          const self = this;
          const {
               handleOpenSnackBarMessage
          } = self.props;
          let url = serviceInfo.HOST + itemServiceConstants.CREATE_ITEM_URI_PATH;
          // Request options
          let options = {
               method: 'POST',
               body: JSON.stringify(this.state.item)
          };
          fetch(url, options)
               .then(function (response) {
                    if (response.status === 200) {
                         handleOpenSnackBarMessage("Malzeme kayıt edildi!", "top", "center", messageTypes.SUCCESS);
                         self.listItems();
                    }
                    self.setState({
                         ...self.state,
                         item: {
                              id: 0,
                              name: "",
                              requiresRecipe: false,
                              status: "Aktif",
                              units: []
                         },
                         dialog: {
                              ...self.state.dialog,
                              createOpen: false
                         }
                    })
               })
               .catch((error => {
                    handleOpenSnackBarMessage(error, "top", "center", messageTypes.ERROR);
                    self.setState({
                         ...self.state,
                         item: {
                              id: 0,
                              name: "",
                              requiresRecipe: false,
                              status: "Aktif",
                              units: []
                         },
                         dialog: {
                              ...self.state.dialog,
                              createOpen: false
                         }
                    })
               }))
     }

     handleCancelNewItemDialog = () => {
          this.setState({
               ...this.state,
               item: {
                    id: 0,
                    name: "",
                    requiresRecipe: false,
                    status: "Aktif",
                    units: []
               },
               dialog: {
                    ...this.state.dialog,
                    createOpen: false
               }
          })
     }

     handleEditItemDialogOpen = (item) => {
          let itemToUpdate = cloneDeep(item);
          this.setState({
               ...this.state,
               item: itemToUpdate,
               dialog: {
                    ...this.state.dialog,
                    editOpen: true
               }
          })
     }

     handleCancelEditItemDialog = () => {
          this.setState({
               ...this.state,
               item: {
                    id: 0,
                    name: "",
                    requiresRecipe: false,
                    status: "Aktif",
                    units: []
               },
               dialog: {
                    ...this.state.dialog,
                    editOpen: false
               }
          })
     }

     handleUpdateItem = () => {
          const self = this;
          const {
               handleOpenSnackBarMessage
          } = self.props;
          let url = serviceInfo.HOST + itemServiceConstants.UPDATE_ITEM_URI_PATH;
          // Request options
          let options = {
               method: 'POST',
               body: JSON.stringify(this.state.item)
          };
          fetch(url, options)
               .then(function (response) {
                    if (response.status === 200) {
                         handleOpenSnackBarMessage("Malzeme güncellendi!", "top", "center", messageTypes.SUCCESS);
                         self.listItems();
                    }
               })
               .catch((error => {
                    handleOpenSnackBarMessage(error, "top", "center", messageTypes.ERROR);
                    this.setState({
                         ...this.state,
                         item: {
                              id: 0,
                              name: "",
                              requiresRecipe: false,
                              status: "Aktif",
                              units: []
                         },
                         dialog: {
                              ...this.state.dialog,
                              editOpen: false
                         }
                    })
               }))

     }

     handleSelectedItem = (event, itemId) => {
          event.preventDefault();
          const {
               items
          } = this.state;
          if (items.length > 0) {
               let itemsLength = items.length;
               for (let itemIndex = 0; itemIndex < itemsLength; itemIndex++) {
                    if (itemId === items[itemIndex].id) {
                         this.setState({
                              ...this.state,
                              selectedItem: items[itemIndex]
                         })
                         break;
                    }
               }
          }
     }

     handleAddUnitToItem = (item, unit) => {
          console.info("Triggered AddUnit");
          let request = {
               itemId: item.id,
               unitId: unit.id
          };
          const self = this;
          const {
               handleOpenSnackBarMessage
          } = self.props;
          let url = serviceInfo.HOST + itemServiceConstants.ADDUNIT_ITEM_URI_PATH;
          // Request options
          let options = {
               method: 'POST',
               body: JSON.stringify(request)
          };
          fetch(url, options)
               .then(function (response) {
                    if (response.status === 200) {
                         handleOpenSnackBarMessage("Malzemeye birim eklendi!", "top", "center", messageTypes.SUCCESS);
                    } else {
                         handleOpenSnackBarMessage("Malzemeye birim eklenemedi!", "top", "center", messageTypes.ERROR);
                    }
                    self.listItems();
               })
               .catch((error => {
                    handleOpenSnackBarMessage("Malzemeye birim eklenemedi!", "top", "center", messageTypes.ERROR);
               }))
     }

     handleRemoveUnitFromItem = (item, unit) => {
          let request = {
               itemId: item.id,
               unitId: unit.id
          }
          const self = this;
          const {
               handleOpenSnackBarMessage
          } = self.props;
          let url = serviceInfo.HOST + itemServiceConstants.REMOVEUNIT_ITEM_URI_PATH;
          // Request options
          let options = {
               method: 'POST',
               body: JSON.stringify(request)
          };
          fetch(url, options)
               .then(function (response) {
                    if (response.status === 200) {
                         handleOpenSnackBarMessage("Malzemeden birim kaldırıldı!", "top", "center", messageTypes.SUCCESS);
                    } else {
                         handleOpenSnackBarMessage("Malzemeden birim kaldırılamadı!", "top", "center", messageTypes.ERROR);
                    }
                    self.listItems();
               })
               .catch((error => {
                    handleOpenSnackBarMessage("Malzemeden birim kaldırılamadı!", "top", "center", messageTypes.ERROR);
               }))
     }

     render() {
          const {
               items,
               rowsPerPage,
               page,
               dialog,
               selectedItem,
               units
          } = this.state;
          const {
               classes
          } = this.props;
          return (
               <div>
                    <Grid container>
                         <Grid item md={12}>
                              <Button onClick={this.handleOpenNewItemDialog} variant="contained" color="primary">
                                   Yeni Malzeme
                              </Button>
                         </Grid>
                         <Grid item md={12}>
                              <ItemTable
                                   classes={classes}
                                   items={items}
                                   rowsPerPage={rowsPerPage}
                                   page={page}
                                   selectedItem={selectedItem}
                                   handleEditItemDialogOpen={this.handleEditItemDialogOpen}
                                   handleChangePage={this.handleChangePage}
                                   handleChangeRowsPerPage={this.handleChangeRowsPerPage}
                                   handleSelectedItem={this.handleSelectedItem}
                              />
                         </Grid>
                         <Grid item md={12}>
                              {
                                   selectedItem.name &&
                                   <div>
                                        <Divider />
                                        <ItemUnitAssociation
                                             classes={classes}
                                             units={units}
                                             item={items.filter(item => item.id === selectedItem.id)[0]}
                                             handleAddUnitToItem={this.handleAddUnitToItem}
                                             handleRemoveUnitFromItem={this.handleRemoveUnitFromItem}
                                        />
                                   </div>
                              }
                         </Grid>
                    </Grid>

                    <NewItemDialog
                         open={dialog.createOpen}
                         item={this.state.item}
                         handleItemInfoChange={this.handleItemInfoChange}
                         handleCreateItem={() => this.handleCreateItem()}
                         handleCancel={this.handleCancelNewItemDialog}
                    />
                    <EditItemDialog
                         open={dialog.editOpen}
                         item={this.state.item}
                         handleItemInfoChange={this.handleItemInfoChange}
                         handleUpdateItem={() => this.handleUpdateItem()}
                         handleCancel={this.handleCancelEditItemDialog}
                    />
               </div>
          )
     }
}

export default withStyles(styles, { withTheme: true })(
     ItemContainer
);