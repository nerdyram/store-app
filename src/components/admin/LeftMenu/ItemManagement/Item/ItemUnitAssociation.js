import React from 'react';
import { Grid, Typography, List, ListItem, Card, Divider, CardContent } from '@material-ui/core';

export const ItemUnitAssociation = (props) => {
     const {
          classes,
          units,
          item,
          handleAddUnitToItem,
          handleRemoveUnitFromItem
     } = props;
     let difference = units;
     if (item.units) {
          difference = units.filter(unit => {
               let tempUnits = item.units.filter(itemUnit => itemUnit.id === unit.id);
               if (tempUnits.length > 0) {
                    return false;
               }
               return true;
          });
     }
     return (
          <Grid container>
               <Grid item md={3}>
                    <Card>
                         <CardContent>
                              <Typography variant="h6">
                                   Birimler
                              </Typography>
                              <List>
                                   {
                                        difference.map((unit, index) => {
                                             return (
                                                  <div key={index}>
                                                       <ListItem button onClick={() => handleAddUnitToItem(item, unit)}>
                                                            {unit.name}
                                                       </ListItem>
                                                       <Divider/>
                                                  </div>
                                             )
                                        })
                                   }
                              </List>
                         </CardContent>
                    </Card>
               </Grid>
               <Grid item md={3}>
                    <Card>
                         <CardContent>
                              <Typography variant="h6">
                                   Eklenen Birimler
                              </Typography>
                              <List>
                                   {
                                        item.units &&
                                        item.units.map((unit, index) => {
                                             return (
                                                  <div key={index}>
                                                       <ListItem button onClick={() => handleRemoveUnitFromItem(item, unit)}>
                                                            {unit.name}
                                                       </ListItem>
                                                       <Divider/>
                                                  </div>
                                             )
                                        })
                                   }
                              </List>
                         </CardContent>
                    </Card>
                    
               </Grid>
          </Grid>
     )
}