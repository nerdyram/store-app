import React from 'react';
import { AppBar, Tabs, Tab  } from '@material-ui/core';

import { GlobalContext } from '../../../context';
import ItemContainer from './Item/ItemContainer';
import UnitContainer from './Unit/UnitContainer';

export const ItemManagement = (props) => {
     const {
          classes,
          handleChangeItemManagementTab,
          activeTab
     } = props;
     return (
          <GlobalContext.Consumer>
               {
                    globalContext => (
                         <div>
                              <AppBar position="static">
                                   <Tabs value={activeTab} onChange={handleChangeItemManagementTab}>
                                        <Tab value={0} label={"Malzemeler"}/>
                                        <Tab value={1} label={"Birimler"}/>
                                   </Tabs>
                              </AppBar>
                              {
                                   activeTab === 0 &&
                                   <ItemContainer handleOpenSnackBarMessage={globalContext.handleOpenSnackBarMessage}/>
                              }
                              {
                                   activeTab === 1 &&
                                   <UnitContainer handleOpenSnackBarMessage={globalContext.handleOpenSnackBarMessage}/>
                              }
                         </div>
                    )
               }               
          </GlobalContext.Consumer>
     )
}