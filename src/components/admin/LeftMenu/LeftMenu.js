import React from 'react';

import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import DashboardIcon from '@material-ui/icons/Dashboard';
import ItemIcon from '@material-ui/icons/Store';
import OrderIcon from '@material-ui/icons/ShoppingBasket';
import AccountIcon from '@material-ui/icons/Person';

import { LeftMenuContext } from '../context';

export const LeftMenu = (props) => {
     return (
          <div>
               <LeftMenuContext.Consumer>
                    {
                         leftMenuContext => (
                              <List>
                                   <ListItem button onClick={leftMenuContext.handleDashboardOpen}>
                                        <ListItemIcon><DashboardIcon/></ListItemIcon>
                                        <ListItemText primary={"İş Analizi"} />
                                   </ListItem>
                                   <Divider/>
                                   <ListItem button onClick={leftMenuContext.handleItemOpen}>
                                        <ListItemIcon><ItemIcon/></ListItemIcon>
                                        <ListItemText primary={"Malzeme Yönetimi"} />
                                   </ListItem>
                                   <ListItem button onClick={leftMenuContext.handleOrderOpen}>
                                        <ListItemIcon><OrderIcon/></ListItemIcon>
                                        <ListItemText primary={"Sipariş Yönetimi"} />
                                   </ListItem>
                                   <ListItem button onClick={leftMenuContext.handleAccountOpen}>
                                        <ListItemIcon><AccountIcon/></ListItemIcon>
                                        <ListItemText primary={"Hesap Yönetimi"} />
                                   </ListItem>
                              </List>
                         )
                    }
               </LeftMenuContext.Consumer>
          </div>
     )
}
