const drawerWidth = 300;

export const styles = theme => ({
     root: {
          display: 'flex',
     },
     adminPanelHeader: {
          margin: "5px",
     },
     adminPanelHeaderShift: {
          marginLeft: "0px",
          marginTop: "5px",
          marginBottom: "5px",
     },
     appBar: {
          transition: theme.transitions.create(['margin', 'width'], {
               easing: theme.transitions.easing.sharp,
               duration: theme.transitions.duration.leavingScreen,
          }),
     },
     appBarShift: {
          width: `calc(100% - ${drawerWidth}px)`,
          marginLeft: drawerWidth,
          transition: theme.transitions.create(['margin', 'width'], {
               easing: theme.transitions.easing.easeOut,
               duration: theme.transitions.duration.enteringScreen,
          }),
     },
     menuButton: {
          margin: "5px",
     },
     hide: {
          display: 'none',
     },
     leftMenu: {
          width: drawerWidth,
          flexShrink: 0,
     },
     leftMenuPaper: {
          width: drawerWidth,
     },
     leftMenuHeader: {
          display: 'flex',
          alignItems: 'center',
          padding: 8,
          // necessary for content to be below app bar
          ...theme.mixins.toolbar,
          justifyContent: 'flex-end',
     },
     content: {
          flexGrow: 1,
          padding: 24,
          transition: theme.transitions.create('margin', {
               easing: theme.transitions.easing.sharp,
               duration: theme.transitions.duration.leavingScreen,
          }),
          marginLeft: -drawerWidth,
     },
     contentShift: {
          flexGrow: 1,
          padding: 24,
          transition: theme.transitions.create('margin', {
               easing: theme.transitions.easing.easeOut,
               duration: theme.transitions.duration.enteringScreen,
          }),
          marginLeft: 0,
     },
     grow: {
          flexGrow: 1
     }
});