import React, { Component } from 'react';

import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import OpenLeftMenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import HomePageIcon from '@material-ui/icons/HomeOutlined';

import { LeftMenu } from './LeftMenu/LeftMenu';
import { LeftMenuContext } from './context';
import DashboardContainer from './LeftMenu/Dashboard/DashboardContainer';
import { ItemManagement } from './LeftMenu/ItemManagement/ItemManagement';
import { GlobalContext } from '../context';
import { styles } from './Style';

class AdminContainer extends Component {

     constructor(props) {
          super(props);
          this.state = {
               leftMenuOpen: true,
               panels: {
                    dashboard: true,
                    sentence: false,
                    intent: false,
                    sentenceIntent: false,
                    dialogFlow: false
               },
               itemManagement: {
                    activeTab: 0
               }
          }
     }

     handleLeftMenuOpen = () => {
          this.setState({
               ...this.state,
               leftMenuOpen: !this.state.leftMenuOpen
          })
     };

     whichPanelIsOpen = (dashboard, item, order, account) => {
          this.setState({
               ...this.state,
               panels: {
                    dashboard: dashboard,
                    item: item,
                    order: order,
                    account: account,
               }
          })
     }

     handleDashboardOpen = () => {
          this.whichPanelIsOpen(
               true,
               false,
               false,
               false
          )
     };

     handleItemOpen = () => {
          this.whichPanelIsOpen(
               false,
               true,
               false,
               false
          )
     };

     handleOrderOpen = () => {
          this.whichPanelIsOpen(
               false,
               false,
               true,
               false
          )
     };

     handleAccountOpen = () => {
          this.whichPanelIsOpen(
               false,
               false,
               false,
               true
          )
     };

     handleChangeItemManagementTab = (e, tab) => {
          this.setState({
               ...this.state,
               itemManagement: {
                    activeTab: tab
               }
          })
     }

     getAppBarClass = () => {
          const {
               classes
          } = this.props;
          return this.state.leftMenuOpen ? classes.appBarShift : classes.appBar;
     }

     getOpenLeftMenuButtonClass = () => {
          const {
               classes
          } = this.props;
          return this.state.leftMenuOpen ? classes.hide : classes.menuButton;
     }

     getAdminPanelHeaderClass = () => {
          const {
               classes
          } = this.props;
          return this.state.leftMenuOpen ? classes.adminPanelHeaderShift : classes.adminPanelHeader;
     }

     getContentClass = () => {
          const {
               classes
          } = this.props;
          return this.state.leftMenuOpen ? classes.contentShift : classes.content;
     }

     render() {
          const {
               classes,
               theme
          } = this.props;
          return (
               <GlobalContext.Consumer>
                    {
                         globalContext => (
                              <div className={classes.root}>
                                   <CssBaseline />
                                   <AppBar
                                        position="fixed"
                                        className={this.getAppBarClass()}
                                   >
                                        <Toolbar >
                                             <IconButton
                                                  color="inherit"
                                                  aria-label="open drawer"
                                                  onClick={() => this.handleLeftMenuOpen()}
                                                  edge="start"
                                                  className={this.getOpenLeftMenuButtonClass()}
                                             >
                                                  <OpenLeftMenuIcon />
                                             </IconButton>
                                             <Typography className={classes.adminPanelHeader} variant="h6" noWrap>
                                                  Malzeme Sipariş - Yönetim
                                             </Typography>
                                             <Typography className={classes.grow} variant="h6" noWrap>
                                                  
                                             </Typography>
                                             <IconButton onClick={() => globalContext.handleChangePage(true, false)}>
                                                  <HomePageIcon/>
                                             </IconButton>
                                        </Toolbar>
                                   </AppBar>
                                   <Drawer
                                        className={classes.leftMenu}
                                        variant="persistent"
                                        anchor="left"
                                        open={this.state.leftMenuOpen}
                                        classes={{
                                             paper: classes.leftMenuPaper,
                                        }}
                                   >
                                        <div className={classes.leftMenuHeader}>
                                             <IconButton onClick={this.handleLeftMenuOpen}>
                                                  {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                                             </IconButton>
                                        </div>
                                        <Divider />
                                        <LeftMenuContext.Provider value={
                                             {
                                                  handleDashboardOpen: this.handleDashboardOpen,
                                                  handleItemOpen: this.handleItemOpen,
                                                  handleOrderOpen: this.handleOrderOpen,
                                                  handleAccountOpen: this.handleAccountOpen
                                             }
                                        }>
                                             <LeftMenu/>
                                        </LeftMenuContext.Provider>
                                   </Drawer>
                                   <main
                                        className={this.getContentClass()}
                                   >
                                        <div className={classes.leftMenuHeader} />
                                        {
                                             this.state.panels.dashboard &&
                                             <DashboardContainer/>
                                        }
                                        {
                                             this.state.panels.item &&
                                             <ItemManagement
                                                  handleChangeItemManagementTab={this.handleChangeItemManagementTab}
                                                  activeTab={this.state.itemManagement.activeTab}
                                                  />
                                        }
                                        {
                                             /*this.state.panels.intent &&*/
                                        }
                                        {
                                             /*this.state.panels.sentenceIntent &&*/
                                        }
                                        {
                                             /*this.state.panels.dialogFlow &&*/
                                        }
                                        {
                                             /*this.state.panels.simulation &&*/
                                        }
                                   </main>
                              </div>
                         )
                    }
               </GlobalContext.Consumer>
          )
     }
}

export default withStyles(styles, { withTheme: true }) (
     AdminContainer
);