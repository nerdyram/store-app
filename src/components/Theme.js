import { createMuiTheme } from "@material-ui/core/styles";
import { trTR } from '@material-ui/core/locale';

export const theme = createMuiTheme({
     root: {
          display: 'flex',
     },
     spacing: {
          unit: 8
     },
     typography: {
          useNextVariants: true,
          fontFamily: [
               '-apple-system',
               'BlinkMacSystemFont',
               '"Segoe UI"',
               'Roboto',
               '"Helvetica Neue"',
               'Arial',
               'sans-serif',
               '"Apple Color Emoji"',
               '"Segoe UI Emoji"',
               '"Segoe UI Symbol"',
          ].join(','),
     },
     palette: {
          primary: {
               main: "#007bff",
          },
     }
}, trTR);