import React, { Component } from 'react';

import { withStyles } from '@material-ui/core/styles';
import { Paper, Typography, Grid } from '@material-ui/core';

import { LoginForm } from './LoginForm';
import { styles } from './Style';

import * as accountServiceConstants from '../../constants/accountService';
import * as serviceInfo from '../../constants/serviceInfo';
import * as messageTypes from '../../constants/messageTypes';
 
class LoginContainer extends Component {

	constructor(props) {
		super(props);
		this.state = {
			account: undefined,
			username: "",
			password: ""
		}
	}

	handleLogin = () => {
		const {
			handleOpenSnackBarMessage,
			handleUpdateLoginAccountInfo
		} = this.props;
          let url = serviceInfo.HOST + accountServiceConstants.LOGIN_ACCOUNT_URI_PATH;
          // Request options
          let options = {
               method: 'POST',
               body: JSON.stringify({ 
				username: this.state.username,
				password: this.state.password
			})
          };

          fetch(url, options)
               .then(function (response) {
				if (response.status == 200) {
					return response.json();
				} else {
					handleOpenSnackBarMessage("Kullanıcı adı veya parola hatalı!", "top", "center", messageTypes.ERROR);
					return {};
				}
               }).then((json) => {
                    handleUpdateLoginAccountInfo(json.data);
               }).catch((error => {
				console.error(error);
               }))
	}

	handleUsernameChange = (e) => {
		this.setState({
			...this.state,
			username: e.target.value
		})
	}

	handlePasswordChange = (e) => {
		this.setState({
			...this.state,
			password: e.target.value
		})
	}

	render() {
		const {
			classes
		} = this.props;
		const {
			username,
			password
		} = this.state;
		return (
			<div className={classes.root}>
				<Paper elevation={3} >
					<Grid container>
						<Grid item md={12}>
							<Typography className={classes.loginHeader} variant="h3">Giriş</Typography>
						</Grid>
						<Grid item md={12}>
							<LoginForm
								username={username}
								password={password}
								handleUsernameChange={this.handleUsernameChange}
								handlePasswordChange={this.handlePasswordChange}
								handleLogin={() => this.handleLogin()} 
								className={classes.loginForm}/>
						</Grid>
					</Grid>
				</Paper>
			</div>
		)
	}
}

export default withStyles(styles, { withTheme: true })(LoginContainer);