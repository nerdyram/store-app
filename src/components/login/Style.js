export const styles = theme => ({
     root: {
		position: "fixed",
		maxWidth: "600px",
		maxHeight: "300px",
		top: "50%",
		left: "50%",
		/* bring your own prefixes */
		transform: "translate(-50%, -50%)"
	},
	loginHeader: {
		margin: "24px",
		textAlign: "center"
	},
	loginForm: {
		margin: "24px"
	},
	loginButton: {
		justifyContent: "center"
	}
});