import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import { Box, FormControl, TextField, Button, Grid } from '@material-ui/core';

import { styles } from './Style';

export const LoginForm = (props) => {
     const classes = makeStyles(styles)();
	const {
		handleLogin,
		handleUsernameChange,
		handlePasswordChange
	} = props;
	return (
		<div>
			<Box className={classes.loginForm}>
				<FormControl fullWidth>
					<TextField
						id="username"
						label="Kullanıcı Adı"
						helperText="Lütfen kullanıcı adınızı giriniz."
						variant="outlined"
						onChange={handleUsernameChange}
					/>
				</FormControl>
				<FormControl fullWidth>
					<TextField
						type="password"
						id="password"
						label="Parola"
						helperText="Lütfen parolanızı giriniz."
						variant="outlined"
						onChange={handlePasswordChange}
					/>
				</FormControl>
				<Grid container className={classes.loginButton}>
					<Button onClick={handleLogin} color="primary" variant="contained">
						Giriş Yap
					</Button>
				</Grid>
			</Box>
		</div>
	)
}