const ACCOUNT = "/account"

export const LIST_ACCOUNT_URI_PATH = ACCOUNT + "/list";
export const GET_ACCOUNT_URI_PATH = ACCOUNT + "/";
export const HOME_ACCOUNT_URI_PATH = ACCOUNT;
export const CREATE_ACCOUNT_URI_PATH = ACCOUNT + "/create";
export const UPDATE_ACCOUNT_URI_PATH = ACCOUNT + "/update";
export const LOGIN_ACCOUNT_URI_PATH = ACCOUNT + "/login";
export const LOGOUT_ACCOUNT_URI_PATH = ACCOUNT + "/logout";