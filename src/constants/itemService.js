const ITEM = "/item"

export const LIST_ITEM_URI_PATH = ITEM + "/list";
export const GET_ITEM_URI_PATH = ITEM + "/";
export const HOME_ITEM_URI_PATH = ITEM;
export const CREATE_ITEM_URI_PATH = ITEM + "/create";
export const UPDATE_ITEM_URI_PATH = ITEM + "/update";
export const SEARCH_ITEM_URI_PATH = ITEM + "/search";
export const ADDUNIT_ITEM_URI_PATH = ITEM + "/addUnitToItem";
export const REMOVEUNIT_ITEM_URI_PATH = ITEM + "/removeUnitFromItem";

const UNIT = "/unit"

export const LIST_UNIT_URI_PATH = UNIT + "/list";
export const GET_UNIT_URI_PATH = UNIT + "/";
export const HOME_UNIT_URI_PATH = UNIT;
export const CREATE_UNIT_URI_PATH = UNIT + "/create";
export const UPDATE_UNIT_URI_PATH = UNIT + "/update";