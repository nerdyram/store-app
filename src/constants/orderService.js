const ORDER = "/order"

export const LIST_ORDER_URI_PATH = ORDER + "/list";
export const GET_ORDER_URI_PATH = ORDER + "/";
export const HOME_ORDER_URI_PATH = ORDER;
export const CREATE_ORDER_URI_PATH = ORDER + "/create";
export const UPDATE_ORDER_URI_PATH = ORDER + "/update";
export const GET_LAST_ORDER_AT_CART_URI_PATH = ORDER + "/getLastOrderAtCart";